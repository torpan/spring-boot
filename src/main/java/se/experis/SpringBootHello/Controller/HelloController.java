package se.experis.SpringBootHello.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {


    @GetMapping("/hello")
    public @ResponseBody String Greet(@RequestParam String Name) {
        return "Hello "+Name+"!";
    }

    @GetMapping("/reverse")
    public @ResponseBody String reverseGreet(@RequestParam String Name)
    {
        return "!"+Name+ " Hello";
    }

}